# WeatherReport

#### This is a repository for sample project - Weather Report Viewer (Using OpenWeatherMap 5 days)

## Pages available:
    1. Home Page
    2. 404 Page

## Features Available:
    1. Fetch Weather report
    2. Form which takes input as city & fetch weather report
    3. Ajax laoder - Visible when ajax call starts & Become hidden once api response recieved

## Directory Structure
    - Project
        - src
            - scripts
                - lazyload
                    - controller
                - app.js
                - app.main.ctrl.js
            - styles
                - main.less
            - views
                - 404
                    - 404.html
                - header.html
                - home.html
            - index.html
        package.json

## Setup Project

    - Clone repository from this link - [Repo](git clone https://bitbucket.org/amit-ahire/weatherreport.git)
    - Do "npm install"
    - To start server - "npm start"
    - In Browser hit this URL to visit the page - "http://localhost:8000"

## Instruction

    - Here index.html is the main file which loads at the top whenever users hits the above url
    - Controllers are placed into script/lazyload directory - which can be loaded using lazyload library

## Libraries used:
    - AngularJS 1.7 & It's other libraries
    - AnguarJS Material
    - OCLazyload
    - Http-server
    - Less
    - Material Icons

## API's Used

    - [OpenWeatherMap](https://openweathermap.org/forecast5)

