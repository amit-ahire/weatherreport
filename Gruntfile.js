module.exports = function(grunt) {
    "use strict";
    // var RELOAD_PORT = 35729;
    var dirConfig = {
        src: "src",
        dest: "dist"
    };


    // Project Configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),	// load the package.json
        // specify configuration for each plugin
        dir: dirConfig,
        uglify: {
            options: {
                mangle: false
            },
            // compress it dynamically, then concatenate with grunt concat.

            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "<%= dir.src %>/scripts",
                        dest: "<%= dir.dest %>/scripts/_tmp",
                        src: [
                            "**/*.js"
                        ],
                    },
                    {
                        expand: true,
                        cwd: "<%= dir.src %>/scripts/lazyload/controllers",
                        dest: "<%= dir.dest %>/scripts/lazyload/controllers",
                        src: [
                            "**/*.js"
                        ],
                    },
                    {
                        expand: true,
                        cwd: "<%= dir.src %>/../node_modules/",
                        dest: "<%= dir.dest %>/scripts/_tmp/plugins/",
                        src: [
                            "angular-animate/angular-animate.min.js",
                            "angular-route/angular-route.min.js",
                            "angular-aria/angular-aria.min.js",
                            "angular-sanitize/angular-sanitize.min.js",
                            "angular-material/angular-material.min.js",
                            "oclazyload/dist/ocLazyLoad.min.js"
                        ],
                    },
                    {
                        expand: true,
                        cwd: "<%= dir.src %>/../node_modules/",
                        dest: "<%= dir.dest %>/scripts/_tmp/vendors/",
                        src: [
                            "angular/angular.min.js"
                        ],
                    }
                ]
            }
        },
        minifyHtml: {
            options: {
                removeComments: true,
                collapseWhitespace: true,
                removeEmptyAttributes: true,
                removeCommentsFromCDATA: true,
                removeRedundantAttributes: true,
                collapseBooleanAttributes: true,
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "<%= dir.src %>/views",
                        dest: "<%= dir.dest %>/views",
                        src: [
                            "**/*.html"
                        ],
                    },
                    {
                        '<%= dir.dest %>/index.html': '<%= dir.dest %>/index.html'
                    }
                ]
            }
        },

        copy: {
            dist: {
                expand: true,
                cwd: "<%= dir.src %>",
                dest: "<%= dir.dest %>",
                src: [
                    "*",
                    "views/**/*",
                    "scripts/lazyload/**/*"
                ],
            }
        },
        // Clean the distribution folder.
        clean: {
            dist: {
                src: ["<%= dir.dest %>"]
            },
            tmp: {
                src: ["<%= dir.dest %>/scripts/_tmp"]
            }
        },

        less: {
            options: {
                cleancss: true
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "<%= dir.src %>/styles",
                        dest: "<%= dir.dest %>/styles",
                        src: "main.less",
                        ext: ".min.css"
                    },
                    {
                        expand: true,
                        cwd: "<%= dir.dest %>/../node_modules/angular-material",
                        dest: "<%= dir.dest %>/styles",
                        src: "angular-material.min.css",
                        ext: ".min.css"
                    }
                ]
            },
            dev: {
                files: [
                    {
                        expand: true,
                        cwd: "<%= dir.src %>/assets/styles",
                        dest: "<%= dir.src %>/_tmp",
                        src: "main.less",
                        ext: ".min.css"
                    }

                ]
            }
        },

        concat: {
            options: {
                separator: grunt.util.linefeed + '\n' + grunt.util.linefeed
            },
            vendors: {
                src: [
                    "<%= dir.dest %>/scripts/_tmp/vendors/angular/*.js"
                ],
                dest: "<%= dir.dest %>/scripts/vendors.js"
            },
            plugins: {
                src: ["<%= dir.dest %>/scripts/_tmp/plugins/**/*.js"],
                dest: "<%= dir.dest %>/scripts/plugins.js"
            },
            custom: {
                src: [
                    "<%= dir.dest %>/scripts/_tmp/common/*.js",
                    "<%= dir.dest %>/scripts/_tmp/app.js",
                    "!<%= dir.dest %>/scripts/_tmp/vendors/*.js",	// already done
                    "!<%= dir.dest %>/scripts/_tmp/plugins/*.js",	// already done
                    "!<%= dir.dest %>/scripts/_tmp/lazyload/**/*.js"	// exclude this also, because these folder's script are loaded dynamically.
                ],
                dest: "<%= dir.dest %>/scripts/app.js"
            }

        },

        // remove production files links from index.html
        processhtml: {
            options: { },
            files: {
                expand: true,
                cwd: "<%= dir.src %>",
                dest: "<%= dir.dest %>",
                src: [
                    "index.html"
                ],
            }

        }

    });

    // Load  all grunt plugins
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-processhtml");
    grunt.loadNpmTasks("grunt-ng-constant");
    grunt.loadNpmTasks("grunt-minify-html");

    // Default Task (that can be run by typing only "grunt" in cmd)
    grunt.registerTask("default", []);
    grunt.registerTask("cleanBuild", ["clean:dist"]);
    grunt.registerTask('build', function(){
        grunt.task.run(["clean:dist", "copy:dist", "less:dist", "uglify", "concat", "clean:tmp", "processhtml", "minifyHtml"]);
    });
    grunt.registerTask("build1", []);
    grunt.registerTask("dev", ["less:dist"]);
    grunt.registerTask("html", ["processhtml"]);
    grunt.registerTask('minhtml', ['minifyHtml']);
};