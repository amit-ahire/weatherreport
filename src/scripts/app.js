;(function () {
    "use strict";

    angular.module("myApp", [
        /* Angular modules */
        "ngRoute",
        "ngAnimate",
        "ngAria",
        "ngMaterial",

        "oc.lazyLoad"

    ])

    .config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('');
    }])


        .config(function ($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    })

        // lazy loading scripts refernces of angular modules only
        .config(["$ocLazyLoadProvider", function ($oc) {
            /*$oc.config({

                debug: _ENV.debug,
                event: false,
                modules: [
                    {
                        name: "",
                        files: []
                    }
                ]
            })*/
        }])

        // route provider
        .config(["$routeProvider", function ($routeProvider) {

            $routeProvider.when("/home", {
                templateUrl: "views/home.html",

                resolve: {
                    deps: ["$ocLazyLoad", function (a) {
                        return a.load(['scripts/lazyload/controllers/app.home.ctrl.js'])
                    }]
                }
            });

            $routeProvider.when("/404", {
                templateUrl: "views/404/404.html"
            });

            $routeProvider
                .when("/", {redirectTo: "/home"})
                .otherwise({redirectTo: "/404"});


        }])


}())
